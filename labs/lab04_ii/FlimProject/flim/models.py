from django.db import models
class Album(models.Model):
    mainActor = models.CharField(max_length = 250)
    filmTitle = models.CharField(max_length = 350)
    director = models.CharField(max_length = 300)
    genre = models.CharField(max_length = 250)
    album_logo = models.CharField(max_length = 1000)
class movie(models.Model):
    album = models.ForeignKey(Album,
on_delete=models.CASCADE)
file_type = models.CharField(max_length = 10)
movie_title = models.CharField(max_length = 250)
