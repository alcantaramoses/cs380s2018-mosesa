import sqlite3
from class_cata import loop
import urllib.request, json
conn = sqlite3.connect(':memory:') #cata.bd is replace so can refresh

c = conn.cursor()
#dropTableStatement = "DROP TABLE loop_4_11"
#c.execute(dropTableStatement)
#-------------------#table loop_4_11----------------------------------
c.execute("""CREATE TABLE loop_4_11 (
            marker text,
            clock1 integer,
            clock2 integer,
            clock3 integer,
            clock4 integer,
            clock5 integer,
            clock6 integer,
            clock7 integer,
            clock8 integer,
            clock9 integer,
            clock10 integer,
            latitude real,
            longitude real
            )""")
#--------------Functions-------------------------------------------------------
def insert_cat(cat):
    with conn:
        c.execute("INSERT INTO loop_4_11 VALUES (:marker,  :clock1, :clock2, :clock3, :clock4,  :clock5,  :clock6, :clock7,  :clock8,  :clock9,  :clock10, :latitude, :longitude)",
        {'marker':cat.marker, 'clock1':cat.clock1, 'clock2':cat.clock2, 'clock3':cat.clock3,
          'clock4':cat.clock4, 'clock5':cat.clock5, 'clock6':cat.clock6, 'clock7':cat.clock7,
          'clock8':cat.clock8, 'clock9':cat.clock9, 'clock10':cat.clock10,'latitude':cat.latitude,
           'longitude':cat.longitude})

#def get_emps_by_name(lastname):
    pass

def update_time(cat, clock1):
    with conn:
        c.execute("""UPDATE loop_4_11 SET clock1 = :clock1
        WHERE latitude = :latitude AND longitude = :longitude""", {'clock1': cat.clock1, 'latitude': cat.latitude,
         'longitude': cat.longitude} )



def remove_cat(cat):
    with conn:
        c.execute("DELETE from loop_4_11 WHERE marker = :marker AND clock1 = :clock1",
                  {'marker': cat.marker, 'clock1': cat.clock1})
#----------------------------------------loop stops(runing at everyhour from 4pm to 2:38Am with some expections)---
#-------------------------------------------
null = 0
S1 = loop('Allegheny_Commons', 1600, 1700, 1800, 1900, 2000, 2100, 2201, 2300, 120, 240, 41.649771, -80.142560)
S2 = loop('North_Village', 1601, 1701, 1801, 1901, 2001, 2101, 2201, 2301, 120, 240, 41.649771, -80.142560)
S3 = loop('Brooks_walk&Main', 1605, 1705, 1805, 1905, 2005, 2105, 2205, 2305, 120, 240, 41.649771, -80.142560)
S4 = loop('College_Court', 1606, 1706, 1806, 1906, 2006, 2106, 2206, 2306, 120, 240, 41.649771, -80.142560)
S5 = loop('Loomis&Main', 1607, 1707, 1807, 1907, 2007, 2107, 2207, 2309, 120, 240, 41.649771, -80.142560)
S6 = loop('Downtown_Mall', 1610, 1710, 1810, 1910, 2010, 2110, 2210, 2312, 120, 240, 41.649771, -80.142560)
S7 = loop('Wal-mart', 1625, 1725, 1825, 1925, 2025, 2125, 2225, null, null, null, 41.649771, -80.142560)
S8 = loop('Chestnut&Park_Ave', 1645, 1745, 1845, 1945, 2045, 2145, 2245, null, null, null, 41.649771, -80.142560)
S9 = loop('Lomis&Park_Ave', 1648, 1748, 1848, 1948, 2048, 2148, 2248, null, null, null, 41.649771, -80.142560)
S10 = loop('Brooks_Hall', 1650, 1750, 1850, 1950, 2050, 2150, 2250, null, null, null, 41.649771, -80.142560)

#S3 = loop('North_age', 6101, 4149771, -.142560)

#S3 = loop('North_age', 101, 4149771, -.142560)
#print(S2)
insert_cat(S1)
insert_cat(S2)
insert_cat(S3)
insert_cat(S4)
insert_cat(S5)
insert_cat(S6)
insert_cat(S7)
insert_cat(S8)
insert_cat(S9)
insert_cat(S10)
#print(S1)
#print(S2)
#print(S3)
#print(S4)
#print(S5)
#print(S6)
#print(S7)
#print(S8)
#print(S9)
#print(S10)
#c.execute("INSERT INTO loop_4_11 VALUES('ALLegheny Commons', 1600, 41.652039, -80.140526 )")
#conn.commit()
def get_cata():
    c.execute("SELECT * FROM loop_4_11")
    print(c.fetchall())
get_cata()

c.execute("SELECT * FROM loop_4_11 WHERE marker = 'North_Village'")
print(c.fetchone())#return the frist row

conn.commit()
conn.close()
#Google MapsDdirections API endpoint
endpoint = 'https://maps.googleapis.com/maps/api/directions/json?'
api_key = 'AIzaSyASpqJ-5IhQ947EDY5TGJRFbYtN94eddOc'
#Asks the user to input Where they are and where they want to go.
origin = input('Where are you?: ').replace(' ','+')
destination = input('Where do you want to go?: ').replace(' ','+')
#Building the URL for the request
nav_request = 'origin={}&destination={}&key={}'.format(origin,destination,api_key)
request = endpoint + nav_request
#Sends the request and reads the response.
response = urllib.request.urlopen(request).read()
#Loads response as JSON
directions = json.loads(response)
print(directions)
